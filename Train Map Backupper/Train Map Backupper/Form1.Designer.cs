﻿
namespace Train_Map_Backupper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SelectLocationPanel = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BackupFolder = new System.Windows.Forms.TextBox();
            this.Source = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.BackupButton = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.SelectLocationPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // SelectLocationPanel
            // 
            this.SelectLocationPanel.Controls.Add(this.button4);
            this.SelectLocationPanel.Controls.Add(this.label2);
            this.SelectLocationPanel.Controls.Add(this.label1);
            this.SelectLocationPanel.Controls.Add(this.BackupFolder);
            this.SelectLocationPanel.Controls.Add(this.Source);
            this.SelectLocationPanel.Controls.Add(this.button2);
            this.SelectLocationPanel.Controls.Add(this.button1);
            this.SelectLocationPanel.Location = new System.Drawing.Point(92, 12);
            this.SelectLocationPanel.Name = "SelectLocationPanel";
            this.SelectLocationPanel.Size = new System.Drawing.Size(423, 135);
            this.SelectLocationPanel.TabIndex = 0;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(102, 88);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(211, 32);
            this.button4.TabIndex = 6;
            this.button4.Text = "Save";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Backup Folder:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Source:";
            // 
            // BackupFolder
            // 
            this.BackupFolder.Location = new System.Drawing.Point(83, 49);
            this.BackupFolder.Name = "BackupFolder";
            this.BackupFolder.Size = new System.Drawing.Size(215, 20);
            this.BackupFolder.TabIndex = 3;
            // 
            // Source
            // 
            this.Source.Location = new System.Drawing.Point(83, 23);
            this.Source.Name = "Source";
            this.Source.Size = new System.Drawing.Size(215, 20);
            this.Source.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(313, 47);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(91, 21);
            this.button2.TabIndex = 1;
            this.button2.Text = "Browse";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(313, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 21);
            this.button1.TabIndex = 0;
            this.button1.Text = "Browse";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BackupButton
            // 
            this.BackupButton.Location = new System.Drawing.Point(61, 168);
            this.BackupButton.Name = "BackupButton";
            this.BackupButton.Size = new System.Drawing.Size(485, 44);
            this.BackupButton.TabIndex = 1;
            this.BackupButton.Text = "Backup";
            this.BackupButton.UseVisualStyleBackColor = true;
            this.BackupButton.Click += new System.EventHandler(this.button3_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(227, 220);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(142, 17);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "Shutdown when finished";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(610, 249);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.BackupButton);
            this.Controls.Add(this.SelectLocationPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Train Map Backup Software";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SelectLocationPanel.ResumeLayout(false);
            this.SelectLocationPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel SelectLocationPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox BackupFolder;
        private System.Windows.Forms.TextBox Source;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button BackupButton;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}

