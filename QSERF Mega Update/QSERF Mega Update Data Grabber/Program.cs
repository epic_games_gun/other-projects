﻿using System;
using System.IO;
using System.Threading.Tasks;
using ParseHub.Client;
using ParseHub.Client.Models;

namespace QSERF_Mega_Update_Data_Grabber
{
    class Program
    {

        private static ParseHub.Client.ProjectRunner projectRunner = new ProjectRunner("tsec8884UacR");
        private static Project project = new Project();
        public static async Task<string> GetTrelloData()
        {
            var RunToken = projectRunner.RunProject("tbHJJ3MQRm2k").RunToken;
            while (!await RunDone(RunToken))
            {
                await Task.Delay(10);
            }
            return new ProjectRunner("tsec8884UacR").GetRun(RunToken).Data;
        }
        private static string MainDirectory = "C:\\Users\\Administrator\\Desktop\\QSERF";
        private static async Task<bool> RunDone(string RunID)
        {
            return new ProjectRunner("tsec8884UacR").GetRun(RunID).DataReady;
        }
        static async Task Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Getting Data...   " + DateTime.Now.ToString("F"));
                string Data = await GetTrelloData();
                Console.WriteLine("Updating Data...   " + DateTime.Now.ToString("F"));
                File.WriteAllText(MainDirectory + "\\updatedata.txt",Data);
                Console.WriteLine("Cooling down...   " + DateTime.Now.ToString("F"));
                await Task.Delay(TimeSpan.FromMinutes(10));
            }
        }
    }
}
