﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using QSERFMegaUpdateTrelloAPI;

namespace QSERF_Mega_Update
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

       

        /// <summary>
        ///     [0] is current progress, [1] is the total progress needed
        /// </summary>
        /// <param name="progress"></param>
        /// <returns></returns>
        private int[] GetStatus(string progress)
        {
            var ReturnList = new List<int>();
            ReturnList.Add(int.Parse(progress.Split('/')[0]));
            ReturnList.Add(int.Parse(progress.Split('/')[1]));
            return ReturnList.OfType<int>().ToArray();
        }

        private string GetDescriptionMainFeature(string Name)
        {
            string Return = "";
            foreach (var s in QSERFUpdate.MainFeatures)
            {
                if (s.Name == Name)
                {
                    foreach (var checklist in s.Description)
                    {
                        Return += "\n" + checklist.Name;
                    }
                    break;
                }
            }

            return Return;
        }

        private string GetDescriptionSideFeature(string Name)
        {
            string Return = "";
            foreach (var s in QSERFUpdate.SideFeatures)
            {
                if (s.Name == Name)
                {
                    foreach (var checklist in s.Description)
                    {
                        Return += "\n" + checklist.Name;
                    }
                    break;
                }
            }

            return Return;
        }

        private string GetDescriptionBuildingChange(string Name)
        {
            string Return = "";
            foreach (var s in QSERFUpdate.BuildingChanges)
            {
                if (s.Name == Name)
                {
                    foreach (var checklist in s.Description)
                    {
                        Return += "\n" + checklist.Name;
                    }
                    break;
                }
            }

            return Return;
        }

        private string GetProgressMainFeature(string Name)
        {
            string Return = "";
            foreach (var s in QSERFUpdate.MainFeatures)
            {
                if (s.Name == Name)
                {
                    Return = s.Progress;
                    break;
                }
            }

            return Return;
        }

        private string GetProgressSideFeature(string Name)
        {
            string Return = "";
            foreach (var s in QSERFUpdate.SideFeatures)
            {
                if (s.Name == Name)
                {
                    Return = s.Progress;
                    break;
                }
            }

            return Return;
        }


        private string GetProgressBuildingChange(string Name)
        {
            string Return = "";
            foreach (var s in QSERFUpdate.BuildingChanges)
            {
                if (s.Name == Name)
                {
                    Return = s.Progress;
                    break;
                }
            }

            return Return;
        }


        private QserfMegaUpdateTrello QSERFUpdate;
        private async void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            TotalUpdateProgressBar.IsIndeterminate = true;
            var UpdateData = QserfMegaUpdateTrello.FromJson(new WebClient().DownloadString("http://qserf.bigheados.com/updatedata.txt"));
            QSERFUpdate = UpdateData;
            TotalUpdateProgressBar.IsIndeterminate = false;
            string[] ComboItems = new[] { "Main Features", "Building Changes", "Side Features" };
            foreach (string comboItem in ComboItems)
            {
                CategoryBox.Items.Add(comboItem);
            }
            CategoryBox.DropDownClosed += (o, args) =>
            {
                UpdateProgressListBox.Items.Clear();
                if (CategoryBox.Text == "Main Features")
                {
                    string Data = "";
                    UpdateCategoryLabel.Content = "";
                    foreach (MainFeature updateDataMainFeature in UpdateData.MainFeatures)
                    {
                        UpdateProgressListBox.Items.Add(updateDataMainFeature.Name);
                    }
                    UpdateCategoryLabel.Content = Data;
                }
                else if (CategoryBox.Text == "Building Changes")
                {
                    string Data = "";
                    UpdateCategoryLabel.Content = "";
                    foreach (BuildingChange updateDataMainFeature in UpdateData.BuildingChanges)
                    {
                        UpdateProgressListBox.Items.Add(updateDataMainFeature.Name);
                    }
                    UpdateCategoryLabel.Content = Data;
                }
                else if (CategoryBox.Text == "Side Features")
                {
                    string Data = "";
                    UpdateCategoryLabel.Content = "";
                    foreach (var updateDataMainFeature in UpdateData.SideFeatures)
                    {
                        UpdateProgressListBox.Items.Add(updateDataMainFeature.Name);
                    }
                    UpdateCategoryLabel.Content = Data;
                }
            };
            UpdateProgressListBox.SelectionChanged += (o, args) =>
            {
                try
                {
                    DescriptionRichTextBox.Document.Blocks.Clear();
                    if (CategoryBox.Text == ComboItems[0])
                    {
                        DescriptionRichTextBox.Document.Blocks.Add(new Paragraph(new Run(GetDescriptionMainFeature(UpdateProgressListBox.SelectedItem.ToString()))));
                        string Progress = GetProgressMainFeature(UpdateProgressListBox.SelectedItem.ToString());
                        ProgressLabel.Content = Progress;
                        int[] ProgressSplit = { Int32.Parse(Progress.Split('/')[0]), Int32.Parse(Progress.Split('/')[1]) };
                        CurrentProgress.Maximum = ProgressSplit[1];
                        CurrentProgress.Value = ProgressSplit[0];
                    }
                    else if (CategoryBox.Text == ComboItems[1])
                    {
                        DescriptionRichTextBox.Document.Blocks.Add(new Paragraph(new Run(GetDescriptionBuildingChange(UpdateProgressListBox.SelectedItem.ToString()))));
                        string Progress = GetProgressBuildingChange(UpdateProgressListBox.SelectedItem.ToString());
                        ProgressLabel.Content = Progress;
                        int[] ProgressSplit = { Int32.Parse(Progress.Split('/')[0]), Int32.Parse(Progress.Split('/')[1]) };
                        CurrentProgress.Maximum = ProgressSplit[1];
                        CurrentProgress.Value = ProgressSplit[0];
                    }
                    else if (CategoryBox.Text == ComboItems[2])
                    {
                        DescriptionRichTextBox.Document.Blocks.Add(new Paragraph(new Run(GetDescriptionSideFeature(UpdateProgressListBox.SelectedItem.ToString()))));
                        string Progress = GetProgressSideFeature(UpdateProgressListBox.SelectedItem.ToString());
                        ProgressLabel.Content = Progress;
                        int[] ProgressSplit = { Int32.Parse(Progress.Split('/')[0]), Int32.Parse(Progress.Split('/')[1]) };
                        CurrentProgress.Maximum = ProgressSplit[1];
                        CurrentProgress.Value = ProgressSplit[0];
                    }
                }
                catch (Exception)
                {
                    CurrentProgress.Maximum = 0;
                    CurrentProgress.Value = 0;
                }
            };
            while (true)
            {
                var UpdateProgress = "";
                foreach (var buildingChange in UpdateData.QserfMegaupdate) UpdateProgress = buildingChange.Progress;
                var TotalUpdateProgress = GetStatus(UpdateProgress);
                TotalUpdateProgressBar.Maximum = TotalUpdateProgress[1];
                TotalUpdateProgressBar.Value = TotalUpdateProgress[0];
                TotalUpdateProgressLabel.Content = "Total Update Progress";
                TotalUpdateProgressLabel.Content += "\n" + TotalUpdateProgress[0] + "/" + TotalUpdateProgress[1];
                await Task.Delay(TimeSpan.FromMinutes(10));
                UpdateData = QserfMegaUpdateTrello.FromJson(new WebClient().DownloadString("http://qserf.bigheados.com/updatedata.txt"));
            }
        }
    }
}